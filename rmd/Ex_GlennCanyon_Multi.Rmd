---
title: "CNet example Multiple subnetworks for Glenn Canyon soil analysis Project"
output: 
  github_document
        
---
<style>
div.red pre { background-color:lightred; }
div.red pre.r { background-color:lightblue; }
</style>

```{r setup, include=FALSE}
library(CNet)
`%ni%` <- Negate(`%in%`)
mainDir=getwd()
knitr::opts_chunk$set(echo = TRUE,eval=FALSE,error=TRUE, cache = TRUE,cache.path = "../cache/",dev.args=list(bg='white'))
#CODE TO RENDER TO MD and HTML
#rmarkdown::render("rmd/Ex_GlennCanyon_Multi.Rmd",output_file=paste0(mainDir,"/Example_Glenn_Multi_EvalFalse.html"));
```
### Bitbucket Documentation

This is an example of executiong some of the CNet functionality, 
data from Glenn Canyon soil analyis Nicholas Bokulich lab at Northern Arizona University
Paper link
in colaboration with the Gilbert Lab at the University of Chicago http://gilbertlab.com


### Project goal

Create a co-occurence network using WGCNA rubust correlation metrics and explore what CNet can tell you about it

### First step load the data

Load your data usually you want to limit that otherwise networks are too large an incomprehensible.
Input files are a text file with otu counts, includes otuid and taxonomy like `biom convert -i table.biom -o table.txt –to-tsv`. File could be compressed. Second a mapping file also with the QIIME format that includes a SampleID column first

```{r read}
#read data files, here the format for reading files in the example package
otufile=system.file("extdata", "glen_canyon_merged_table.taxa.0.0001.txt", package = "CNet")
mapfile=system.file("extdata", "glen_canyon_merged_table.map", package = "CNet")

otu_table=read_otu_table(otufile)
map_table=read_map_table(mapfile)
#explore your data OTUID and taxonomy must be present
head(otu_table$OTUID)
head(otu_table$taxonomy)
table(map_table$Type)
#clean up otu tables that are going to be used for networks, remove soil crust samples 
otu_clean=otu_table[otu_table$taxonomy!="Unclassified",]
samples_sc=map_table$SampleID[map_table$Type=="SoilCrust"]
otu_base=otu_clean[colnames(otu_clean) %ni% samples_sc ]
```

Build multiple networks one per site name in the dataset and do analysis similar to the single network
the difference you will have a list of networks and will call different functions (usually ending with _list)
Muiltiple networks dont have the option to be visualized in the screen, all the outputs go
to the disk, however, you can always call the single functions in a loop or sapply if you wish.
```{r mnet1}
g_sites=create_WGCNA_network_by_column("Glenn-All",otu_base,map_table,"Site_Name")
```

As before `plot_and_print_summary_list` is a good function to explore your subnetworks, also counting edges between different phyla. 
```{r mst1.1}
stats=plot_and_print_summary_list(g_sites)
lapply(print_collapsed_edge_count_list(g_sites,summary_attrib = "phylum"),head)
```

From the prior step `plot_and_print_summary_list` you select your favorite format and further explore your data. 
Also you can manage the scale of colors for the continuous environmental variable plot. This is a good idea when plotting multiple subnetworks
and you want to keep the color scale consistent across all figures. This is going to the disk.
```{r mst1.2}
formatvariable = "layout.kamada.kawai"
plot_network_by_continuous_variable_list(g_sites,formatvariable=formatvariable,colorvariable=colorvariable,mincategorycolor = 3633,maxcategorycolor = 3743)
```

For an ilustration, here we are plotting the first site continuous plot for first site: "Halls"
```{r mst1.3}
colorvariable="estimated_elevation"
g_sites=add_weighted_category_vertex_attribute_list(g_sites,otu_base,map_table,colorvariable)
plot_network_by_continuous_variable(g_sites[[1]],formatvariable=formatvariable, colorvariable=colorvariable, toScreen=T)
```

Here we are plotting the first site categorical plot for first site: "Halls". This plot colors and labels by default by the OTU `phylum` but you can always change the `color_attrib` and `label_attrib`.
```{r mst1.4}
plot_network_by_categorical_variable(g_sites[[1]],formatvariable=formatvariable,label_attrib = NA,scale_label = 0.2,scale_vertex = 0.3,toScreen=T)
```

Here we are plotting the communities for first site "Halls". By default this will color and label by the `membership`. Here reducing the label scale to something very small so the figure is not too busy.
```{r mst1.5}
g_site1_w_membership=calculate_cluster_attribs_and_plot(g_sites[[1]], formatvariable=formatvariable, scale_label=0.001, toPlot=T, toScreen=T )
```

It is possible to calculate statistics at the sample level, by isolating the otus that belong to each sample, one sample at the time. This can allow you to compare network stats with other data at the sample level. This will produce a two level list of data, first the site level and for each site the statitics at the sample level. 
```{r mst2.1}
g_sites_sampleids=split_network_per_samples_list(g_sites,otu_base, map_table, "Site_Name")
```

Calculates stats for each sample for each sampleid
```{r mst2.2}
stats2=sapply(g_sites_sampleids,get_summary_stats_list)
```

Here metric summaries and phylum interconnection counts.
```{r mst2.3,eval=FALSE}
stats2_col_phy=sapply(g_sites_sampleids,print_collapsed_edge_count_list,summary_attrib="phylum")
```

Also you can collapse the network at different levels, according to a property of the OTU table. For instance
phylum or any variable that you load to the network at the vertex (OTU) level See `add_vertex_attribute` and `add_vertex_attribute_list`. 
Here collpasing a list of networks and showing the first site collpased at the class level.

```{r mst3}
#creating a collpased network at phylum level
g_sites_phy=collapse_network_list(g_sites,"phylum")
plot_network_with_abundance(g_sites_phy[[1]],scale_label = 0.7,scale_vertex = 0.002, scale_edges = 0.01, 
                            label_attrib="phylum", color_attrib = "phylum", toScreen=T)

```






