% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/cavnet_fx_io.R
\name{read_otu_table}
\alias{read_otu_table}
\title{Create an OTU dataframe}
\usage{
read_otu_table(otu_filename)
}
\arguments{
\item{otu_filename}{This is a flat file with otu information. First column is expect to be "#OTU ID"
last column is expected to be 'taxonomy', same as the flat file table.txt output from
biom convert -i table.biom -o table.txt --to-tsv}
}
\value{
dataframe object
}
\description{
Create an OTU dataframe
}
