# README #

CAVNet is an R package that facilitates __C__reation __A__nalysis and __V__isualization of __Net__works.
It has been mostly used in the context of microbial ecology in companion with [QIIME](doi:10.1038/nmeth.f.303) datasets, but the basic functionality
applies to any type of count data or any existing igraph networks. This documentation will provide examples of microbial dataset produced via QIIME and non-microbial datasets as well.
You are expected to be familiar with igraph, this is in a way an extension of that functionality.

By default it would use data produced from the biom convert command, where OTU table has a first column #OTU ID and a last column taxonomy

Here there is an example of QIIME work prior to run CAVNet

```
single_rarefaction.py -i otu_table_no_singletons_meta.biom -o otu_table_no_singletons_meta61638.biom -d 61638

filter_otus_from_otu_table.py -i otu_table_no_singletons_meta61638.biom -o otu_table_no_singletons_meta61638_0.0001.biom --min_count_fraction 0.0001

biom summarize-table -i otu_table_no_singletons_meta61638_0.0001.biom 

biom convert -i table.biom -o table.from_biom_w_taxonomy.txt --to-tsv --header-key taxonomy
```

# IGraph Tutorials #

Manual pages igraph and graph.structure are good places to start.
Type in demo(package="igraph") (or use igraphdemo if you have tcltk ) to get a list of demos. 
Documentation of Igraph could be found in the link http://igraph.org/r/#rdoc1

Another excellent tutorial of network theory and igraph could be found at the website of Katherine Ognyanova (aka Katya) Assistant Professor at the School of Communication and Information at Rutgers University. http://kateto.net/networks-r-igraph


# CAVNet network creation #

CAVNet incorporates a simple network inference method based on Pearson or Spearman correlations, [WGCNA](DOI: 10.1186/1471-2105-9-559). WGCNA uses a [fast and robust Persons correlation and hierarchical clustering implementation](DOI: 10.18637/jss.v046.i11). The included implementation added corrections for multiple tests, deconvolution to remove indirect connections and a random matrix method to estimate the best co-ocurrence threshold. The rights for this implementation belong to Jianming Xu, College of Environmental and Resource Sciences, Zhejiang University, 866 Yuhangtang Road, Hangzhou 310058, China and the code can be seen in the bitbucket repository [location](R/Create_Network_WGCNA.R).

Any additional co-ocurrence network creation methods could be used along with CAVNet, the only requirement is that the network produced is an igraph object. By running the command `initialize_igraph_network` allows to initialize the non WGCNA network with the CAVNet required attributes required to run the additional functionality.

The default input file is an OTU table in text format. You can refer to functions `read_otu_table` for more details. 


### Developing Team ###

* Lead developer Cesar Cardona cesarcardona AT uchicago DOT edu
* With the support of the gilbert lab at the University of Chicago http://gilbertlab.com

### Installation ###

`library(devtools)`

`install_bitbucket("cesarcardonau/cavnet@release", build_vignettes=TRUE)`

`library(CAVNet)`


### Citations ###

```
#!r

Cardona Cesar, CAVNet, (2017), Bitbucket repository, https://bitbucket.org/cesarcardonau/cavnet
```

BibTeX
```
#!r

@misc{Cardona2017,
  author = {Cardona, Cesar},
  title = {CAVNet: Creation Analysis and Visualization of Networks Package},
  year = {2017},
  publisher = {Bitbucket},
  journal = {Bitbucket repository},
  url= {https://bitbucket.org/cesarcardonau/cavnet},
  commit = {000012}
}
```

### Documentation  ###

This is an example of executing some of the CAVNet functionality, data
from Glenn Canyon soil analysis Nicholas Bokulich lab at Northern Arizona
From paper [link](coming_soon) 

Documentation of functions can be seen by running
`help(package="CAVNet")`
Detailed examples can be seen by running 
`vignette("introduction",package="CAVNet")` 

### Analyzing igraph networks produced by another package other than CAVNet ###

Run the function `initialize_igraph_network`
```
sparCC=initialize_igraph_network(net0 = iGraph_From_SparCC, projectname = "Himalayas", parameters = "R=0.1")

```
