#!/usr/bin/env Rscript
#################################################################################
##Microbial Ecology Lab - CAVNet
##PI:Jack Gilbert
##Code:Cesar Cardona
##2016-
#################################################################################

### Initialize and create CAVNet networks########################################
#' Initialize an igraph network with the basic attributes required for CAVNet
#'
#' @param projectname String to describe the project - used for output files
#' @param parameters String to describe the parameters used creating that specific igraph network
#' @export initialize_igraph_network
initialize_igraph_network <-function(net0,projectname="projectname",parameters="parameters")
{
  net0<-set.graph.attribute(net0, "net",projectname)
  net0<-set.graph.attribute(net0, "subnet","overall")
  net0<-set.graph.attribute(net0, "pvalue_thres",parameters)
  net0<-set.graph.attribute(net0, "splitcolumn","overall")
  V(net0)$OTUID=V(net0)$name
  return(net0)
}

#' Create network calling WGCNA package from OTU data frame
#'
#' @param projectname String, optional attribute to uniquely name your network. It will be loaded
#' in the network as a graph attribute (net) and will be printed in the report files
#' @param otu_table Data frame, This is data frame object loaded from a OTU object.
#' As oppose to the function create_WGCNA_network_from_file here you have the option to
#' manipulate the otu_table object before calling this function.
#' Remember read_otu_table could create an otu_table dataframe for you.
#' @param method String, either `spearman` or `pearson`. `spearman` is the default value
#' @param pvalue_thres Numeric, threshold for significant correlations
#' @param debugit Boolean, prints and creates files with additional execution information if set to TRUE
#' @return igraph net work object
#' @export create_WGCNA_network
create_WGCNA_network <-function(projectname="untitled",otu_table,method="spearman",pvalue_thres=0.01,debugit=F) {
  otu2=otu_to_otu2(otu_table)
  otu2.t<- as.data.frame(t(otu2[,1:(ncol(otu2)-10)]))

  comm=as.data.frame(t(as.matrix(otu2[,1:(ncol(otu2)-10)])))
  comm <- comm[ order(row.names(comm)), ]

  #print debug information
  print(paste("CCNote: Comm dimensions :",nrow(comm),"x",ncol(comm)))
  method_name=paste0(substr(method,1,2),pvalue_thres)

  net0<-buildnetwork(comm,projectname,method,pvalue_thres,debugit = debugit)
  net0<-set.graph.attribute(net0, "net",projectname)
  net0<-set.graph.attribute(net0, "subnet","overall")
  net0<-set.graph.attribute(net0, "pvalue_thres",method_name)
  net0<-set.graph.attribute(net0, "splitcolumn","overall")
  net0<-load_taxa_with_otu2(net0,otu2,otu2.t)
  net0<-load_network_stats(net0)
  print_network_stats(net0)
  return(net0)
}
create_WGCNA_network_by_column <-function(projectname="untitled",otu_table,map_table,splitcolumn,
                                          method=method,pvalue_thres=0.01,debugit=F) {
  otu_tables=create_otu2_tables(otu_table,map_table)
  otu2=otu_tables[[1]]
  otu2.t=otu_tables[[2]]
  otu2.map=otu_tables[[3]]
  #slices the rows in groups according to the split column
  df <- split(otu2.map, otu2.map[,c(splitcolumn)])
  gsl=lapply(df, function(otu2.map.subgroup) {
    #debug print
    subgroup0=as.character(otu2.map.subgroup[,c(splitcolumn)][1])
    group_name=paste0(projectname,".",splitcolumn,".",subgroup0)
    method_name=paste0(substr(method,1,2),pvalue_thres)
    print(group_name)
    #get data for only current subgroup
    otu2.t.subgroup=otu2.map.subgroup[,1:ncol(otu2.t)]
    colnames(otu2.t.subgroup)=colnames(otu2.map.subgroup)[1:ncol(otu2.t)]
    otu2.t.subgroup[,1:ncol(otu2.t.subgroup)]=sapply(otu2.t.subgroup[,1:ncol(otu2.t.subgroup)], as.character)
    otu2.t.subgroup[,1:ncol(otu2.t.subgroup)]=sapply(otu2.t.subgroup[,1:ncol(otu2.t.subgroup)], as.numeric)
    otu2.t.subgroup=(otu2.t.subgroup[,colSums(abs(otu2.t.subgroup)) !=0])
    var_cols=apply(otu2.t.subgroup, 2, var)
    counts_table_var_gt0=otu2.t.subgroup[,as.integer(var_cols)>0]
    g0=buildnetwork(counts_table_var_gt0,group_name,method,pvalue_thres,debugit)
    net0<-set.graph.attribute(g0, "net",projectname)
    net0<-set.graph.attribute(net0, "subnet",subgroup0)
    net0<-set.graph.attribute(net0, "pvalue_thres",method_name)
    net0<-set.graph.attribute(net0, "splitcolumn",splitcolumn)
    ##load taxa
    net0<-load_taxa_with_otu2(net0,otu2,counts_table_var_gt0)
    print_network_stats(net0)
    return(net0)
  })
  gsl=load_network_stats_list(gsl)
  return(gsl)
}
