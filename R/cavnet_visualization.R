#!/usr/bin/env Rscript
#################################################################################
##Microbial Ecology Lab - CAVNet
##PI:Jack Gilbert
##Code:Cesar Cardona
##2016-
#################################################################################
library(ggplot2)
library(RColorBrewer)
library(igraph)
### Definitions #################################################################
##add triangle shape to igraph library
##from documentation http://finzi.psych.upenn.edu/library/igraph/html/shapes.html
mytriangle <- function(coords, v=NULL, params) {
  vertex.color <- params("vertex", "color")
  if (length(vertex.color) != 1 && !is.null(v))
    vertex.color <- vertex.color[v]
  vertex.size <- 1/200 * params("vertex", "size")
  if (length(vertex.size) != 1 && !is.null(v))
    vertex.size <- vertex.size[v]
  symbols(x=coords[,1], y=coords[,2], bg=vertex.color,
          stars=cbind(vertex.size, vertex.size, vertex.size),
          add=TRUE, inches=FALSE)
}

##########################default plot and stats network###############################
explore_network_with_taxa_list <- function(gsl0,
                                        formatstyle0=c("layout.fruchterman.reingold","layout.kamada.kawai"),
                                        scale_label=0.10,scale_vertex=1){
  print(paste("CCNote: Running default report. Num nets=",length(gsl0)))
  outputname=get_output_net_name(gsl0[[1]])
  netstats0=get_summary_stats_list(gsl0)
  gstats0=netstats0[[1]]
  vstats0=netstats0[[2]]
  plot_degree_count(outputname,vstats0)

  sapply(gsl0, function(g0) {
    print_collapsed_edge_count(g0)
    sapply(formatstyle0, function(x) {
      print(x)
      plot_network_by_categorical_variable(g0,formatvariable=x,
                                           scale_label = scale_label,scale_vertex = scale_vertex) #defaults to phylum
    })
  })
  return(netstats0)
}
#' Runs a first exploratory analysis of your network. Counts inter-kigdom connections.
#' Plots degree couts. Plot network in two different formats: fruchterman.reingold and kamada.kawai
#'
#' @param gs0 igraph object
#' @param formatstyle0 list of styles to use for network plots
#' @param scale_label factor to rescale the text in labels
#' @param scale_vertex factor to rescale the size of vertices
#' @return a list with four dataframes with different stats (graph level, vertex level, edge level, mean of vertex level)
#' @export explore_network_with_taxa
explore_network_with_taxa <- function(gs0,
                                   formatstyle0=c("layout.fruchterman.reingold","layout.kamada.kawai"),
                                   scale_label=0.10,scale_vertex=1){
  gsl <- list()
  gsl[[1]]=gs0
  return(explore_network_with_taxa_list(gsl,formatstyle0,
                              scale_label,scale_vertex))
}
#' Print a network with has abundance attributes for vertex and edges, those will affect the tickness of the
#' edges
plot_network_with_abundance <- function(g0,formatvariable="layout.fruchterman.reingold",
                                        color_attrib=NA,shape_attrib="kingdom",label_attrib=NA,
                                        scale_label=0.35,scale_vertex=1,scale_edges=0.15,
                                        brewer_palette_name="Set1",toScreen=F) {
  print(paste0("CCNote: creating abundance plot. Color='",color_attrib,"'. Labels= '",label_attrib,"'..."))
  plot_network_by_categorical_variable(g0,formatvariable,
                color_attrib,shape_attrib,size_attrib = "abundance",label_attrib=label_attrib,thickness_attrib = "abundance",
                scale_label=scale_label,scale_vertex=scale_vertex,scale_edges=scale_edges,
                brewer_palette_name = brewer_palette_name,
                toScreen=toScreen
                )
}

plot_network_with_abundance_list <- function(gsl,formatvariable="layout.fruchterman.reingold",
                                             color_attrib=NA,shape_attrib="kingdom",label_attrib=NA,
                                             scale_label=0.35,scale_vertex=1,scale_edges=0.15,
                                             brewer_palette_name="Set1") {
  return(sapply(gsl,function(x){ plot_network_with_abundance(g0=x,formatvariable,
                                                      color_attrib,shape_attrib,label_attrib,
                                                      scale_label,scale_vertex,scale_edges,
                                                      brewer_palette_name = brewer_palette_name)}))
}
#' Plot OTUS and color them according to a continuous attribute associated to each OTU
#' It either creates a pdf file with the figure or send it to
#' the screen depending in the parameter toScreen, defaults to FALSE.
#'
#' @param g0 igraph object
#' @param formatvariable igraph style to use for network plots, needs to be given in character format. See manual pages for "layout.*" from http://igraph.org/r/doc/ for full list
#' @param color_attrib vertex attribute to color the nodes in the plot. Needs to be a continuous integer value.
#' @param shape_attrib vertex attribute to assign shapes for nodes, with a maximum of three values.
#' @param size_attrib vertex attribute to assign size for nodes, defaults to the vertex degree
#' @param scale_label factor to rescale the text in labels. Default 0.1
#' @param scale_vertex factor to rescale the node (vertex) size. Default 1
#' @param scale_edges factor to rescale the size (thickness) of edges. Default 0.7
#' @param scale_arrow factor to rescale the size of arrows. Default 0.4
#' @param mincolorvalue describes the minimum value to color in the colorbar scale, by default looks at the minimum value of the continuous variable
#' @param maxcolorvalue describes the maximum value to color in the colorbar scale, by default looks at the maximum value of the continuous variable
#' @param toScreen TRUE or FALSE, with the FALSE option (default) a pdf file is saved to disk
#' @export plot_network_by_continuous_variable
plot_network_by_continuous_variable <- function(g0,formatvariable="layout.fruchterman.reingold",
                                                color_attrib,shape_attrib="kingdom",size_attrib="degree",
                                            scale_label=0.1,scale_vertex=1,scale_edges=0.70,scale_arrow=0.40,
                                            mincolorvalue=min(get.vertex.attribute(g0,color_attrib)),
                                            maxcolorvalue=max(get.vertex.attribute(g0,color_attrib)),toScreen=F) {
  outputname=get_output_subnet_name(g0)
  if(!toScreen)
    pdf(paste0(outputname,".",formatvariable,".",color_attrib,".pdf"),10,10)
  print(paste0("CCNote: creating '",color_attrib,"' network..."))
  print(paste0("CCNote: continuous variable to color"," min=",mincolorvalue," max=",maxcolorvalue))
  print(paste0("CCNote: format: '",formatvariable,"' class: '",class(formatvariable),"'"))
  palf <- colorRampPalette(c("#800026", "#FED000"))
  colors=palf(maxcolorvalue-mincolorvalue+1)
  V(g0)$color=colors[get.vertex.attribute(g0,color_attrib)-mincolorvalue+1]
  layout(matrix(c(2,1), nrow=1), c(9,1))
  par(mar=c(4, 1, 4, 1),mgp=c(3,1,0))
  color.bar(colors, mincolorvalue,maxcolorvalue,10,title = paste0("\n\n\n\n",color_attrib))
  par(mar=c(1, 1, 1, 1))
  plot_network(g0,formatvariable,
               label_attrib=NA,shape_attrib,size_attrib,thickness_attrib=NA,
               scale_label,scale_vertex,scale_edges,scale_arrow)
  if(!toScreen)
    dev.off()
}

plot_network_by_continuous_variable_list <- function(gsl,formatvariable="layout.fruchterman.reingold",
                                                     color_attrib, shape_attrib="kingdom",size_attrib="degree",
                                                scale_label=0.1,scale_vertex=1,scale_edges=0.70,scale_arrow=0.40,
                                                mincolorvalue=min(sapply(gsl, function(x) {min(get.vertex.attribute(x,color_attrib))})),
                                                maxcolorvalue=max(sapply(gsl, function(x) {max(get.vertex.attribute(x,color_attrib))}))) {
  return(sapply(gsl,function(x){ plot_network_by_continuous_variable(x, formatvariable,
                                                                      color_attrib, shape_attrib, size_attrib,
                                                                      scale_label,scale_vertex,scale_edges,scale_arrow,
                                                                      mincolorvalue,
                                                                      maxcolorvalue)}))

}

#DO NOT USE DIRECTLY. Internal plotting function, colors need to be set before calling this function, either by continous or categorical variables
#Not to be confused with same function name from phyloseq
plot_network <- function(g0,formatvariable="layout.fruchterman.reingold",
                                  label_attrib=NA,shape_attrib=NA,size_attrib="degree",thickness_attrib=NA,
                                  scale_label=0.3,scale_vertex=1,scale_edges=0.70,scale_arrow=0.40,
                                  vertex_dist=0) {

  # clips as a circle
  igraph::add_shape("triangle", clip=shapes("circle")$clip,
                    plot=mytriangle)
  #set shape
  if(is.null(get.vertex.attribute(g0,shape_attrib)[1])){
    V(g0)$shape=c("circle")
    levelsshapes="none"
  } else{
    shapefac=as.factor(get.vertex.attribute(g0,shape_attrib))
    levelsshapes=levels(shapefac)
    V(g0)$shape=c("square","circle","triangle","triangle")[shapefac]#should match in the legend below pch=c(15,16,17,17) below
    #V(g0)$shape=c("circle","triangle","triangle")[shapefac]#should match in the legend below pch=c(15,16,17,17) below
  }
  #set size and label
  maxsize=max(get.vertex.attribute(g0,size_attrib))
  V(g0)$size=round((get.vertex.attribute(g0,size_attrib)*7*scale_vertex)/maxsize,1)
  if( !is.na(thickness_attrib))
    E(g0)$width=get.edge.attribute(g0,thickness_attrib)*scale_edges
  else
    E(g0)$width=scale_edges
  if( !is.na(label_attrib))
    V(g0)$label=get.vertex.attribute(g0,label_attrib)
  else
    V(g0)$label=NA
  set.seed(14)
  #calculate location of nodes based on format given
  coords <- do.call(formatvariable, list(g0))
  #set plotting settings
  igraph.options(plot.layout=coords,
       vertex.label.dist=vertex_dist,vertex.label.cex=scale_label,edge.width=0.375,
       vertex.frame.color="black",rescale=FALSE,edge.curved=0.0,
       edge.arrow.size=scale_arrow)
  #tkplot(g0)
  # Now plot the points on the existing window
  plot(g0)
   if(length(levelsshapes)>1)
     legend('bottom',levelsshapes[1:length(levelsshapes)],pch=c(15,16,17,17)[1:length(levelsshapes)],cex = 0.71,col="black")
     #legend('bottom',levelsshapes[1:length(levelsshapes)],pch=c(16,17,17)[1:length(levelsshapes)],cex = 0.71,col="black")

  #set plotting defaults back again
  igraph.options(plot.layout=layout.fruchterman.reingold,
                  vertex.label.dist=0,vertex.label.cex=1,edge.width=0.375,
                  vertex.size=20,vertex.color="gold",vertex.frame.color="black",
                 edge.width=2,edge.arrow.size=0.5,rescale=TRUE)
}
#####################plot color by categorical value ###############################
#color_attrib_sequence: provides a fix pattern of colors, for instances when you have multiple subnetworks with a subset of
#elements from a bigger set. We want to color to be consistet across all plots
plot_network_by_categorical_variable_return_p <- function(g0,formatvariable="layout.fruchterman.reingold",
                                                 color_attrib=NA,shape_attrib=NA,size_attrib="degree",label_attrib=NA,thickness_attrib=NA,
                                                 scale_label=0.1,scale_vertex=1,scale_edges=0.70,scale_arrow=0.70,
                                                 vertex_dist=0,
                                                 color_attrib_sequence=NA,
                                                 brewer_palette_name="RdYlGn") {
  getPalette = colorRampPalette(brewer.pal(9,brewer_palette_name ))
  numvertex=length(V(g0))
  levelscolors=colorint=1
  if(is.na(color_attrib) && numvertex <=20)
    V(g0)$color <- getPalette(numvertex)
  else if(is.na(color_attrib) && numvertex > 20)
    V(g0)$color <- "gold"
  else if(!is.character(color_attrib_sequence)) {
      colorint=as.factor(get.vertex.attribute(g0,color_attrib))
      levelscolors=levels(colorint)
      print("CCNote: check color... ")
      print(table(levelscolors[colorint]==get.vertex.attribute(g0,color_attrib)))##quick check
  } else {
      colorint=match(get.vertex.attribute(g0,color_attrib),color_attrib_sequence)
      levelscolors=color_attrib_sequence
  }
  palettecolors=getPalette(length(levelscolors))
  V(g0)$color=palettecolors[colorint]
  p0=plot_network(g0,formatvariable,
               label_attrib,shape_attrib,size_attrib,thickness_attrib,
               scale_label,scale_vertex,scale_edges,scale_arrow, vertex_dist)
  if(!is.na(color_attrib))
    legend("bottomright",legend=levelscolors,pch = 16,cex=0.71, col = palettecolors)
  return(p0)
}
#' Plot OTUS and color them according to a categorical attribute associated to each OTU
#' It either creates a pdf file with the figure or send it to
#' the screen depending in the parameter toScreen, defaults to FALSE.
#'
#' @param g0 igraph object
#' @param formatvariable igraph style to use for network plots, needs to be given in character format. See manual pages for "layout.*" from http://igraph.org/r/doc/ for full list
#' @param color_attrib vertex attribute to color the nodes in the plot. Needs to be a continuous integer value.
#' @param shape_attrib vertex attribute to assign shapes for nodes, with a maximum of three values.
#' @param size_attrib vertex attribute to assign size for nodes, defaults to the vertex degree
#' @param label_attrib vertex attribute to assign text label for nodes, defaults to phylum
#' @param thickness_attrib edge attribute to assign size for nodes, defaults to no attribute, so thickness is define by `scale_edges` value alone
#' @param scale_label factor to rescale the text in labels. Default 0.1
#' @param scale_vertex factor to rescale the node (vertex) size. Default 1
#' @param scale_edges factor to rescale the size (thickness) of edges. Default 0.4
#' @param color_attrib_sequence list the values from the `color_attrib` to use for coloring.
#' @param brewer_palette_name defines the name of the brewer palette to use http://colorbrewer2.org/#type=sequential&scheme=BuGn&n=3
#' @param toScreen TRUE or FALSE, with the FALSE option (default) a pdf file is saved to disk
#' @export plot_network_by_categorical_variable
plot_network_by_categorical_variable <-function(g0,formatvariable="layout.fruchterman.reingold",
                        color_attrib="phylum",shape_attrib="kingdom",size_attrib="degree",label_attrib="phylum",thickness_attrib=NA,
                        scale_label=0.1,scale_vertex=1,scale_edges=0.1,
                        color_attrib_sequence=NA,
                        brewer_palette_name="RdYlGn",toScreen=F) {
  print(paste0("CCNote: creating '",color_attrib,"' colored plot. Labels='",label_attrib,"'..."))
  outputname=get_output_subnet_name(g0)
  if(!toScreen)
    pdf(paste0(outputname,".",formatvariable,".",color_attrib,".pdf"),12,10)
  plot_network_by_categorical_variable_return_p(g0,formatvariable,
              color_attrib,shape_attrib,size_attrib,label_attrib,thickness_attrib,
              scale_label,scale_vertex,scale_edges,
              color_attrib_sequence=color_attrib_sequence,
              brewer_palette_name=brewer_palette_name)
  if(!toScreen)
    dev.off()


}
plot_network_by_categorical_variable_list <- function(gsl,formatvariable="layout.fruchterman.reingold",
                                      color_attrib="phylum", shape_attrib="kingdom",size_attrib="degree",label_attrib="phylum",thickness_attrib=NA,
                                      scale_label=0.1,scale_vertex=1,scale_edges=0.1,
                                      color_attrib_sequence=NA,
                                      brewer_palette_name="RdYlGn") {
  return(sapply(gsl,function(x){ plot_network_by_categorical_variable(g0=x,formatvariable,
                                                                color_attrib,shape_attrib,size_attrib,label_attrib,thickness_attrib,
                                                                scale_label,scale_vertex,scale_edges,
                                                                color_attrib_sequence=color_attrib_sequence,
                                                                brewer_palette_name=brewer_palette_name)}))
}

###############plot betweenness boxplot############################################
plot_betweenness <- function(outputname,vstats) {
  print("CCNote: Creating betweenness plot...")
  p <- ggplot(vstats, aes(1,betweenness))
  p1 <- p + geom_boxplot(aes(fill = "Global Network")) + xlab("Global Network")+ylim(0,20000)
  ggsave(paste0(outputname,".betweenness.pdf"))
}
###############plot counts/degree plot############################################
plot_degree_count <- function(outputname,vstats) {
  print("CCNote: Creating degree count plots...")
  #plot of counts/degree for degree greater than 0
  counts=aggregate(rep(1,length(vstats$degree)), by=list(Category=vstats$subnet,vstats$degree), FUN=sum)
  counts=counts[(counts$Group.2>0),]
  print("Aggregated Data Degree vs Count")
  print(counts[1:5,])
  p <- ggplot(counts, aes(Group.2,x,color=Category))+geom_point()
  p1=p + xlab("Degree")+ ylab("Count")
  ggsave(paste(outputname,".degree.pdf",sep=""),width=7.5,height=5)
  p2=p + geom_point()+ xlab("Degree - log10 scale")+ ylab("Count - log 10 scale")+
    scale_x_log10(
      breaks = scales::trans_breaks("log10", function(x) 10^x),
      labels = scales::trans_format("log10", scales::math_format(10^.x))
    ) +
    scale_y_log10(
      breaks = scales::trans_breaks("log10", function(x) 10^x),
      labels = scales::trans_format("log10", scales::math_format(10^.x))
    )
  ggsave(paste(outputname,".degree.log.pdf",sep=""),width=7.5,height=5)
}

#' Calculates the communities (modules of a network)
#'
#' @export get_clusters_and_plot
get_clusters_and_plot <- function(g0, formatvariable="layout.fruchterman.reingold",
                                               label_attrib = "membership",
                                               scale_label=0.3,scale_vertex=1,
                                               brewer_palette_name = 'Set1',toPlot=T, toScreen=F) {
  print(paste("CCNote: Creating cluster plot... "))
  #create clusters with prefered method, I chosen walktrap
  wc <- cluster_walktrap(g0)
  #other options
  #wc <- cluster_spinglass(g0) NO
  #wc <- cluster_label_prop(g0)
  #wc <- cluster_fast_greedy(g0)
  #print clusters info to flat files and dendogram.pdf
  outputname=get_output_subnet_name(g0)
    write.table(sizes(wc),paste0(outputname,".clusters.size.txt"), quote = FALSE,row.names = FALSE)
  sapply(1:length(wc), function(i) { cat("cluster",i,"****",
                                         wc[[i]], "\n", file=paste0(outputname,".clusters.txt"), append=TRUE)})
  #print dendrogram
  pdf(paste(outputname,".clusters.dendrogram.pdf",sep=""),10,200)
  igraph.options(vertex.label.cex=0.05)
  plot_dendrogram(wc)
  dev.off()
  #print network and clusters
  V(g0)$membership=wc$membership
  if(toPlot)
    plot_network_by_categorical_variable(g0,formatvariable = formatvariable,
                                         color_attrib = "membership",label_attrib = label_attrib,
                                         scale_label=scale_label,scale_vertex=scale_vertex,
                                         brewer_palette_name = brewer_palette_name,
                                         toScreen = toScreen)
  return(g0)
}
###############plot color bar############################################
color.bar <- function(lut, min, max=-min, nticks=11, ticks=round(seq(min, max, len=nticks)), title='') {
  scale = (length(lut)-1)/(max-min)

  #dev.new(width=1.75, height=5)
  plot(c(0,10), c(min,max), type='n', bty='n', xaxt='n', xlab='', yaxt='n', ylab='', main=title, cex.main=0.75)
  axis(2, ticks, las=1)
  for (i in 1:(length(lut)-1)) {
    y = (i-1)/scale + min
    rect(0,y,10,y+1/scale, col=lut[i], border=NA)
  }
}

###############FORMATS############################################
#layouts = c("layout.random",
#"layout.circle",
#"layout.sphere",
#"layout.fruchterman.reingold",
#"layout.kamada.kawai",
#"layout.reingold.tilford",
#"layout.drl",
#"layout.lgl",
#"layout.graphopt")
################END##############
